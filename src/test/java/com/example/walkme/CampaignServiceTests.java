package com.example.walkme;

import com.example.walkme.model.Campaign;
import com.example.walkme.services.CampaignService;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CampaignServiceTests {
    private static String CAMPAIGN_NAME = "The name of the campaign";

    @Test
    public final void campaignCreated() {
        CampaignService campaignService = new CampaignService();
        Campaign campaign = new Campaign(CAMPAIGN_NAME);
        assertEquals(campaignService.getAll().size(), 0);
        campaignService.create(campaign);
        List<Campaign> campaigns = campaignService.getAll();
        assertEquals(campaigns.size(), 1);
        assertEquals(campaigns.get(0), campaign);
    }

    @Test
    public final void campaignIdCreated() {
        CampaignService campaignService = new CampaignService();
        Campaign campaign = new Campaign(CAMPAIGN_NAME);
        assertEquals(campaign.getId(), 0);
        campaignService.create(campaign);
        assertNotEquals(campaign.getId(), 0);
    }

    @Test
    public final void availableCampaignsMaxTotal() {
        int USER_ID = 1;
        String SECOND_CAMPAIGN = CAMPAIGN_NAME + " SEC";
        CampaignService campaignService = new CampaignService();
        Campaign firstCampaign = new Campaign(CAMPAIGN_NAME);
        Campaign secCampaign = new Campaign(SECOND_CAMPAIGN)
                .setMaxTotal(1);
        campaignService.create(firstCampaign);
        campaignService.create(secCampaign);
        assertEquals(campaignService.getAllAvailable(USER_ID).size(), 2);
        List<Campaign> allAvailable = campaignService.getAllAvailable(USER_ID);
        assertEquals(allAvailable.size(), 1);
        assertEquals(allAvailable.get(0), firstCampaign);
    }

    @Test
    public final void availableCampaignsPerUserMaxTotal() {
        int USER_ID = 1;
        String SECOND_CAMPAIGN = CAMPAIGN_NAME + " SEC";
        CampaignService campaignService = new CampaignService();
        Campaign firstCampaign = new Campaign(CAMPAIGN_NAME);
        Campaign secCampaign = new Campaign(SECOND_CAMPAIGN)
                .setMaxPerUser(1);
        campaignService.create(firstCampaign);
        campaignService.create(secCampaign);
        assertEquals(campaignService.getAllAvailable(USER_ID).size(), 2);
        List<Campaign> allAvailable = campaignService.getAllAvailable(USER_ID);
        assertEquals(allAvailable.size(), 1);
        assertEquals(allAvailable.get(0), firstCampaign);
    }
}
