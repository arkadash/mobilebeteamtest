package com.example.walkme;

import com.example.walkme.model.Campaign;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class CampaignTests {

    @Test
    public final void creation() {
        String CAMPAIGN_NAME = "The name of the campaign";
        String CAMPAIGN_OTHER_NAME = "Other Name";
        Campaign campaign = new Campaign(CAMPAIGN_NAME);
        assertEquals(campaign.getName(), CAMPAIGN_NAME);
        assertEquals(campaign.getId(), 0);
        assertNull(campaign.getMaxPerUser());
        assertNull(campaign.getMaxTotal());
        campaign.setName(CAMPAIGN_OTHER_NAME)
                .setMaxPerUser(30)
                .setMaxTotal(300)
                .setId(3);
        assertEquals(campaign.getName(), CAMPAIGN_OTHER_NAME);
        assertEquals(campaign.getId(), 3);
        assertEquals(campaign.getMaxPerUser(), new Integer(30));
        assertEquals(campaign.getMaxTotal(), new Integer(300));
    }

    @Test
    public final void creationWithInvalidTotal() {
        String CAMPAIGN_NAME = "The name of the campaign";
        Campaign campaign = new Campaign(CAMPAIGN_NAME);
        campaign.setMaxTotal(-1);
        assertNull(campaign.getMaxTotal());
        campaign.setMaxTotal(0);
        assertNull(campaign.getMaxTotal());
    }

    @Test
    public final void creationWithInvalidTotalPerUser() {
        String CAMPAIGN_NAME = "The name of the campaign";
        Campaign campaign = new Campaign(CAMPAIGN_NAME);
        campaign.setMaxPerUser(-1);
        assertNull(campaign.getMaxPerUser());
        campaign.setMaxPerUser(0);
        assertNull(campaign.getMaxPerUser());
    }

    @Test
    public final void addIsMaxTotal() {
        String CAMPAIGN_NAME = "The name of the campaign";
        Campaign campaign = new Campaign(CAMPAIGN_NAME);
        assertFalse(campaign.isMax());
        assertEquals(campaign.getCountTotal(), 0);
        campaign.setMaxTotal(2);
        assertFalse(campaign.isMax());
        assertTrue(campaign.add(1));
        assertEquals(campaign.getCountTotal(), 1);
        assertFalse(campaign.isMax());
        assertTrue(campaign.add(1));
        assertEquals(campaign.getCountTotal(), 2);
        assertTrue(campaign.isMax());
        assertFalse(campaign.add(1));
        assertEquals(campaign.getCountTotal(), 2);
    }

    @Test
    public final void addIsMaxPerUser() {
        String CAMPAIGN_NAME = "The name of the campaign";
        int USER_ID = 1;
        Campaign campaign = new Campaign(CAMPAIGN_NAME);
        assertFalse(campaign.isMax());
        assertFalse(campaign.isMaxUser(USER_ID));
        assertEquals(campaign.getCountTotal(), 0);
        assertNull(campaign.getCountPerUser(USER_ID));
        campaign.setMaxPerUser(2);
        assertFalse(campaign.isMaxUser(USER_ID));
        assertTrue(campaign.add(1));
        assertEquals(campaign.getCountPerUser(USER_ID), new Integer(1));
        assertFalse(campaign.isMaxUser(USER_ID));
        assertTrue(campaign.add(1));
        assertEquals(campaign.getCountPerUser(USER_ID), new Integer(2));
        assertTrue(campaign.isMaxUser(USER_ID));
        assertFalse(campaign.add(1));
        assertTrue(campaign.isMaxUser(USER_ID));
        assertEquals(campaign.getCountTotal(), 2);
    }

    @Test
    public final void validName() {
        Campaign campaign = new Campaign(null);
        assertTrue(campaign.getName().length() > 0);
        campaign = new Campaign("");
        assertTrue(campaign.getName().length() > 0);
    }

}
