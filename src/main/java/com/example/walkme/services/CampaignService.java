package com.example.walkme.services;

import com.example.walkme.model.Campaign;
import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class CampaignService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private Map<Integer, Campaign> campaignMap;
    private final AtomicInteger campiagnId;

    public CampaignService() {
        // TODO: add JPA
        this.campaignMap = new HashMap<>();
        this.campiagnId = new AtomicInteger(101);
    }

    public Campaign get(final int id) {
        return this.campaignMap.get(id);
    }

    public List<Campaign> getAll() {
        return new ArrayList<>(this.campaignMap.values());
    }

    public List<Campaign> getAllAvailable(int userId) {
        return this.getAll().stream()
                .filter(campaign -> {
                    return campaign.add(userId);
                })
                .collect(Collectors.toList());
    }

    public Campaign create(final Campaign entity) {
        Preconditions.checkNotNull(entity);
        int entityId = this.campiagnId.getAndIncrement();
        entity.setId(entityId);
        logger.info("Added Entity: " ,entity);
        this.campaignMap.put(entityId, entity);
        return entity;
    }

}
