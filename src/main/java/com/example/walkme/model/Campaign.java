package com.example.walkme.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Campaign {

    private int id;

    private String name = "DEFUALT NAME";

    private Integer maxTotal;

    private Integer maxPerUser;

    private int countTotal;

    private Object data;

    public Campaign(String name) {
        this.setName(name);
    }

    private Map<Integer, Integer> countPerUserMap = new HashMap<>();

    public boolean isMax() {
        return Objects.nonNull(maxTotal) && this.countTotal == maxTotal;
    }

    public boolean isMaxUser(int userId) {
        Integer campaignPerUserCount = this.countPerUserMap.get(userId);
        boolean isUserIdOrMaxNull = Objects.isNull(campaignPerUserCount) ||
                Objects.isNull(this.maxPerUser);
        return !isUserIdOrMaxNull && campaignPerUserCount.equals(this.maxPerUser);
    }

    public boolean add(int userId) {
        boolean isAdded = false;
        if (!this.isMax() && !this.isMaxUser(userId)) {
            this.addToMax();
            this.addToUser(userId);
            isAdded = true;
        }
        return isAdded;
    }

    private void addToMax() {
        this.countTotal++;
    }

    private void addToUser(int userId) {
        if (Objects.isNull(this.maxPerUser)) {
            return;
        }
        this.countPerUserMap.get(userId);
        if (!this.countPerUserMap.containsKey(userId)) {
            this.countPerUserMap.put(userId, 1);
        } else {
            this.countPerUserMap.put(userId, this.countPerUserMap.get(userId) + 1);
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getMaxTotal() {
        return maxTotal;
    }

    public Integer getMaxPerUser() {
        return maxPerUser;
    }

    public int getCountTotal() {
        return countTotal;
    }

    public Integer getCountPerUser(int userId) {
        return this.countPerUserMap.get(userId);
    }

    public Campaign setName(String name) {
        if(Objects.nonNull(name) && name.length() > 0){
            this.name = name;
        }
        return this;
    }

    public Campaign setMaxTotal(Integer maxTotal) {
        if(Objects.nonNull(maxTotal) && maxTotal > 0){
            this.maxTotal = maxTotal;
        }
        return this;
    }

    public Campaign setMaxPerUser(Integer maxPerUser) {
        if(Objects.nonNull(maxPerUser) && maxPerUser > 0){
            this.maxPerUser = maxPerUser;
        }
        return this;
    }

    public void setId(int id) {
        this.id = id;
    }
}
