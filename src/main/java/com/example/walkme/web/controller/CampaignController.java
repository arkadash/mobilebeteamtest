package com.example.walkme.web.controller;

import com.example.walkme.model.Campaign;
import com.example.walkme.services.CampaignService;
import com.example.walkme.web.resources.CampaignResource;
import com.example.walkme.web.resources.ThresholdsResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/campaigns")
@CrossOrigin(origins = "*")
public class CampaignController {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private CampaignService campaignService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    private ResponseEntity<List<CampaignResource>> getUserCampaigns(
            @RequestParam("user_id") int userId) {
        ResponseEntity<List<CampaignResource>> res;
        try {
            List<CampaignResource> campaignResources = campaignService.getAllAvailable(userId)
                    .stream().map(CampaignResource::new)
                    .collect(Collectors.toList());
            res = ResponseEntity.ok(campaignResources);
        } catch (Exception e) {
            this.logger.debug("Failed to send campaigns", e);
            res = ResponseEntity.badRequest().body(Collections.emptyList());
        }
        return res;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    private ResponseEntity<CampaignResource> newCampaign(
            @RequestBody final CampaignResource resource) {
        ResponseEntity<CampaignResource> res;
        try {
            ThresholdsResource thresholds = Objects.isNull(resource.getThresholds()) ? new ThresholdsResource() : resource.getThresholds();
            Campaign created = new Campaign(resource.getName())
                    .setMaxTotal(thresholds.getMaxTotal())
                    .setMaxPerUser(thresholds.getMaxPerUser());
            Campaign campaign = campaignService.create(created);
            res = ResponseEntity.ok(new CampaignResource(campaign));
        } catch (Exception e) {
            this.logger.debug("Failed to send campaigns", e);
            res = ResponseEntity.badRequest().body(null);
        }
        return res;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    private ResponseEntity<List<CampaignResource>> getAllCampaigns() {
        ResponseEntity<List<CampaignResource>> res;
        try {
            List<CampaignResource> campaignResources = campaignService.getAll()
                    .stream().map(CampaignResource::new)
                    .collect(Collectors.toList());
            res = ResponseEntity.ok(campaignResources);
        } catch (Exception e) {
            this.logger.debug("Failed to send campaigns", e);
            res = ResponseEntity.badRequest().body(Collections.emptyList());
        }
        return res;
    }

    @RequestMapping(value = "/max", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    private ResponseEntity<Map<Integer,Integer>> getCampaignsCounts() {
        ResponseEntity<Map<Integer,Integer>> res;
        try {
            Map<Integer, Integer> idsToCurrent = new HashMap<>();
            campaignService.getAll().forEach(campaign -> {
                idsToCurrent.put(campaign.getId(), campaign.getCountTotal());
            });
            res = ResponseEntity.ok(idsToCurrent);
        } catch (Exception e) {
            this.logger.debug("Failed to send campaigns", e);
            res = ResponseEntity.badRequest().body(new HashMap<Integer, Integer>());
        }
        return res;
    }


}
