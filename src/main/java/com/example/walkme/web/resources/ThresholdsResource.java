package com.example.walkme.web.resources;

import com.example.walkme.model.Campaign;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ThresholdsResource {

    public ThresholdsResource() {
    }

    @JsonProperty("max_total")
    private Integer maxTotal;

    @JsonProperty("max_per_user")
    private Integer maxPerUser;

    ThresholdsResource(Campaign campaign) {
        this.maxPerUser = campaign.getMaxPerUser();
        this.maxTotal = campaign.getMaxTotal();
    }

    public Integer getMaxTotal() {
        return maxTotal;
    }

    public Integer getMaxPerUser() {
        return maxPerUser;
    }
}
