package com.example.walkme.web.resources;

import com.example.walkme.model.Campaign;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class CampaignResource {

    public CampaignResource() {
    }

    public CampaignResource(Campaign campaign) {
        this.id = campaign.getId();
        this.name = campaign.getName();
        this.data = new HashMap<>();
        this.thresholds = new ThresholdsResource(campaign);
    }

    @JsonProperty("id")
    private int id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("thresholds")
    private ThresholdsResource thresholds;

    @JsonProperty("data")
    private Map data;

    public String getName() {
        return name;
    }

    public ThresholdsResource getThresholds() {
        return thresholds;
    }

    public Map getData() {
        return data;
    }
}
