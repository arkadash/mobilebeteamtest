package com.example.walkme;

import com.example.walkme.config.SwaggerConfig;
import com.example.walkme.config.WebMvcConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WalkMeApplication {

	public static void main(String[] args) {
        SpringApplication.run(new Class[]{
                WalkMeApplication.class,
                WebMvcConfig.class,
                SwaggerConfig.class
        }, args);
    }
}
