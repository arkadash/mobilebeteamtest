package com.example.walkme.moc;

import com.example.walkme.model.Campaign;
import com.example.walkme.services.CampaignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@Component
public class MocData {

    @Resource
    private Environment environment;

    @Autowired
    private CampaignService campaignService;

    @PostConstruct
    public void init() {
        boolean enableMocData = Boolean.parseBoolean(environment.getRequiredProperty("enable.moc.data"));
        if(enableMocData){
            this.createCampaigns();
        }
    }

    private void createCampaigns() {
        this.campaignService.create(new Campaign("Holiday Specials")
                .setMaxTotal(100)
                .setMaxPerUser(10));
        this.campaignService.create(new Campaign("Rate this app")
                .setMaxPerUser(3));
        this.campaignService.create(new Campaign("First come first served")
                .setMaxTotal(4));

    }
}
